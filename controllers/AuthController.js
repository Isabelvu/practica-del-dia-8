//Creamos un nuevo controller para hacer el login y el logout
const io = require('../io');
const requestJson = require("request-json");


function loginUsersV1(req, res) {
  console.log("POST /apitechu/v1/login");
  console.log(req.body.email);
  console.log(req.body.password);
  //traemos el fichero de usuarios
     var users = require('../usuarios.json');
       for (i = 0; i < users.length; i++)  {
       console.log("El valor del indice es" + i);
       if (users[i].email == req.body.email && users[i].Password == req.body.password)  {
           console.log("coinciden email y contraseña. El Email de usuario es  " + users[i].email)
        //   var userLogged = true;
           users[i].logged = true;
              io.writeUserDatatoFile(users);
              res.send({"msg" : "Usuario logado con exito ID " + users[i].id})
              break;
           } else {
                if  (users.length == users[i].id) {
                  console.log("Pasa por el else. Usuario logado?  " + users[i].logged)
                  res.send({"msg" : "Usuario o contraseña errónea"})
                }
            //
                };
            };
   };


// FUNCION LOGOUT
function logoutUsersV1(req, res) {
  console.log("POST/apitechu/v1/logout/:id");
  console.log(req.body.email);
  console.log(req.body.password);
  //traemos el fichero de usuarios
     var users = require('../usuarios.json');
     for (i = 0; i < users.length; i++)  {
     if (users[i].logged == true && req.params.id == users[i].id)  {
               delete users[i].logged;
               io.writeUserDatatoFile(users);
               res.send({"msg" : "Logout realizado con exito"})
      } else {
             if  (users.length == users[i].id) {
               res.send({"msg" : "Error en el logout"})
           }
   };
}
}


module.exports.loginUsersV1 = loginUsersV1 ;
module.exports.logoutUsersV1 = logoutUsersV1 ;
