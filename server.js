require("dotenv").config();

const express = require('express');
const app = express();

const port = process.env.PORT || 3000;

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");

 next();
}

app.use(express.json());
app.use(enableCORS);


const authController = require('./controllers/AuthController');


app.listen(port);
console.log("API escuchando en el puerto cambio2 " + port);


// post a login
app.post('/apitechu/v1/login',authController.loginUsersV1);
app.post('/apitechu/v1/logout/:id',authController.logoutUsersV1);
